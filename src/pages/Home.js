import React from 'react'

// react bootstrap components
import Container from 'react-bootstrap/Container'


// components
import Banner from './../components/Banner'
import Highlight from './../components/Highlight'

export default function Home(){
	return(
		
		<Container fluid>
			<Banner/>
			<Highlight/>
		</Container>

	)
}