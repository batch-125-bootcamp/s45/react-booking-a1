import React, {useState, useEffect} from 'react'

import{
	Container
} from 'react-bootstrap'

// // arrow function
// const Counter = () => {

// }

export default function Counter(){
const [count, setCount] = useState(0)

useEffect(() => {

	// statement/ logic
	document.title = `You Clicked ${count} times`
	// this will automatically load once and will not rerun again unless changes are made.


},[count]);
// will listen to this, if this will be removed, declared state will run

	return(
		<Container className="m-4">
			<h1>You clicked {count} times</h1>
			<button className="btn btn-info" onClick={ 
				() => {
				setCount(count +1)
			}

		}>Click Me</button>
		</Container>

		)
}