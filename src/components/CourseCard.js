import React from 'react'

import{
	Row,
	Col,
	Card,
	Button

} from 'react-bootstrap'


export default function CourseCard(){

	return(

		<Row className="justify-content-center">
			<Col>
				<Card>
				  <Card.Body>
				    <Card.Title>Course Name</Card.Title>
					   <h5>Description:</h5>
					   <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Provident perspiciatis adipisci accusamus animi fugiat, similique, minus odit alias voluptatem blanditiis velit cupiditate voluptatibus dicta expedita consequuntur nobis ullam magni quae?</p>
					   <h5>Price:</h5>
					   <p>Php 40,000</p>
				    	<Button variant="primary">Enroll</Button>	
				  </Card.Body>
				</Card>
			</Col>`
		</Row>

		)

}