import React from 'react';
import ReactDOM from 'react-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
// bootstrap css (react bootstrap documentation- getting started)

import App from './App';
// import Welcome from './components/Welcome'


// // import './index.css'; 
// import App from './App';
// import reportWebVitals from './reportWebVitals'; *removed doesnt need*

// elements inside the render should be wrap in one element/ container to be read by react.
// the elements are called JSX (JavaScript Extension), looks like an HTML tags but it is really an extension for Javascript.

// old way of writing react elements React.createElement("h1", "attribute", "Hello")

ReactDOM.render(
  <App/>,
  document.getElementById('root')
);

